# selfcare-chatbot

## Usage

API URL: http://18.118.200.40:5002/webhooks/rest/webhook

### POST REQUEST


Post request Body Data JSON format is as follows: <br />
{
    "sender": "test",
    "message": "hi"
}

Expected response: <br />
[
    {
        "recipient_id": "test",
        "text": "Hey! How are you?"
    }
]


## Rasa ChatBot Deployment

### 1. Creating A Docker Image
a] Create a docker image using the following command: <br />
    docker build -t <image_name> . <br />
b] Check if the docker image is running locally using the following command:<br />
    docker run -t -i -p 5002:5002 <image_name><br />
    
### 2. Using AWS ECS for deployment
#### Creating an ECR repository
a] Create an ECR repository <br />
b] Push the created docker image using the commands given in the "view push commands" tab in the ECR repository <br />
#### Creating an ECS cluster
Go to AWS -> ECS — Create Cluster <br />
Select Networking Only option <br />
Provide a name for the cluster <br />
Create a new VPC for the cluster and add atleast two subnets. <br />
Click on Create <br />
#### Create new Task Definition
Create a new Task Definition with launch type as Fargate <br />
In the add container tab, provide an container name and the URI of the ECR repository created earlier. <br />
In the port mappings, add 5002 as the container port. <br />
#### Run a Task
Go to your task definition — Click on Action — Run task <br />
Select the current cluster, number of tasks = 1, and add the public subnets. <br />
Edit the Security Group to add a custom TCP with port range = 5002 <br />
Run Task <br />
The Rasa server is available on the public IP of the task, i.e, <Public IP>:5002
